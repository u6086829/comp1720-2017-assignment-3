/*
Septian Razi
u6086829

Inspired by: 2 Cars app (https://play.google.com/store/apps/details?id=com.ketchapp.twocars&hl=en)
Sounds Used:
    - Car crash : https://www.youtube.com/watch?v=iukXU30cwj4
    - Background Music : https://www.youtube.com/watch?v=vX1xq4Ud2z8&index=6&list=PLKQWNXwX2zTkYUBbLPaTwYpDBIcFnDch3
    - Score Up : https://www.youtube.com/watch?v=GGoYBJvPEdY (not used)

*/

var car1;
var car2;
var now;
var obsLeft = [];
var obsRight = [];
var gameEnd = false;
var carWidth = 50;
var carHeight= 100;
var shuffleWidth = 100;
var obsSpeed = 3;
var score = 0;
var start = 0;
var gap = -(carHeight*2)-60;
var enemyColour = [128,0,0];
var leftLaneRightRoad;
var rightLaneRightRoad;
var leftLaneLeftRoad;
var rightLaneLeftRoad;
var sounds = [];
var y = 0;
var highScore = [];
var carCrashSound;
var bgm;


function preload(){
    //sounds = [loadSound('sounds/carCrash.mp3'), loadSound('sounds/8bitBGM.mp3'), loadSound('sounds/scoreUp.mp3')];
    carCrashSound = loadSound('assets/carCrash.mp3');
    bgm = loadSound('assets/8bitBGM.mp3')
}

function setup() {
    createCanvas(windowWidth, windowHeight);

    bgm.play();

    leftLaneRightRoad = 100+(width/2);
    rightLaneLeftRoad = (width/2)-100;
    rightLaneRightRoad = leftLaneRightRoad + shuffleWidth;
    leftLaneLeftRoad = rightLaneLeftRoad - shuffleWidth;

    // initialising first few obstacles
    initialise();

}

function initialise() {
  car1 = {
            y : height/2,
            onLeftLane: false,
            colour : [217, 209, 24],
            onRightRoad : false};
  car2 = {
            y : height/2,
            onLeftLane: true,
            colour : [47,63,115],
            onRightRoad : true};

  obsLeft[2] = {
            y : gap,
            onLeftLane: true,
            colour : enemyColour,
            onRightRoad : false};

  obsRight[2] = {
            y : gap,
            onLeftLane: true,
            colour : enemyColour,
            onRightRoad : true};

  obsLeft[1] = {
            y : gap*2,
            onLeftLane: false,
            colour : enemyColour,
            onRightRoad : false};

  obsRight[1] = {
            y : gap*2,
            onLeftLane: true,
            colour : enemyColour,
            onRightRoad : true};

  obsLeft[0] = {
            y : gap*3,
            onLeftLane: false,
            colour :enemyColour,
            onRightRoad : false};

  obsRight[0] = {
            y : gap*3,
            onLeftLane: false,
            colour : enemyColour,
            onRightRoad : true};
}


function draw() {
    // your "draw loop" code goes here
    noStroke();
    if (!gameEnd){
      background(23, 136, 18);
      fill(110);
      rect(width/2-(carWidth*2 + 150),0,carWidth*2 + 100,height);
      rect(width/2+50,0,carWidth*2 + 100,height);

      fill(230);
      for (i = 0; i < 20; i++){
        rect((width/2-(carWidth*2+50)),(y+(100*i))-30,10, 50);
        rect((width/2+(carWidth*2+50)),(y+(100*i))-30,10, 50);
      }
      y = ((y + obsSpeed-1) % 100) ;

      fill(255);
      textSize(32);
      textAlign(CENTER);
      text(score,width/2,150);


      drawCar(car1);
      drawCar(car2);
      if (millis() > 6000){
        obsLeft.map(updateObs);
        obsRight.map(updateObs);
        obsLeft.map(drawCar);
        obsRight.map(drawCar);

        obsLeft.map(checkPosition);
        obsRight.map(checkPosition);
        checkDifficulty();
      }
      if (start +6000 > millis()) {
        fill(255);
        strokeWeight(2);
        stroke(0);
        rect(width/2-(carWidth*2+50)-25, height-135,50,50,10,10);
        rect(width/2+(carWidth*2+50)-25, height-135,50,50,10,10);

        noStroke()
        fill(0);
        textAlign(CENTER);
        text('Q', width/2-(carWidth*2+50), height-100);
        text('P', width/2+(carWidth*2+50), height-100);


      }


    } else {
      if (millis() <now + 2100 && millis() > now + 700){
        fill(0,0,0,10);
        rect(0,0,width,height);
      } else if (millis() < now + 4500 && millis() > now + 4000){
        fill(255,255,255,10);
        textSize(40);
        textAlign(CENTER);
        text("You can't do two things at once.", width/2 ,height/2);
      } else if (millis() < now + 6500 && millis() > now + 6000){
        fill(255,255,255,10);
        textSize(30);
        textAlign(CENTER);
        text("#GetYourHandOffIt", width/2 ,height - 100);
      } else if (millis() < now + 7500 && millis() > now + 7000){
        fill(255,255,255,10);
        textSize(20);
        textAlign(CENTER);
        text("High Score", width - 100 ,50);
        highScore.sort(function(a, b){return b - a});

        for (i = 0; i < highScore.length; i++){
          if (i > 3){
            break;
          }
          text(highScore[i], width -100, 80 + 20*i);
        }
      }else if (millis() < now + 9000 && millis() > now + 8500){
          fill(255,255,255,10);
          textSize(10);
          textAlign(CENTER);
          text("Press R to insert coin", width - 100 ,height-50);
        }
      }
    }

// for info on handling key presses, see the FAQ:
// https://cs.anu.edu.au/courses/comp1720/assignments/03-simple-arcade-game/#handling-keypresses

//function to move car from one lane to the other
function shuffleLeft (car){
  if (car.onLeftLane){
    car.onLeftLane = false;
  } else {
    car.onLeftLane = true;
  }
}

//function called when a keyboard key is pressed
function keyPressed() {
    var car;
    if (!gameEnd){
      if (keyCode === 81){
        car = car1;
      } else if (keyCode === 80){
        car = car2;
      }
      shuffleLeft(car);
    } else if (gameEnd) {
        if (keyCode === 82){
          reset();
        }
    }
}

// function to give the obstacles some movement
function updateObs(obstacle){
    obstacle.y += obsSpeed;
}

var doneOnce = false;

//function called to increase difficulty at a certain moment
function checkDifficulty(){
    var difficultyPeriod = 15;
    if (score % difficultyPeriod == 1 && !doneOnce){
      obsSpeed++;
      doneOnce = true;
    } else if (score % difficultyPeriod != 1){
      doneOnce = false;
    }
}

//function to add an obstacle to the left road
function addObstacleLeft(){
  var leftLane = random([false,true]);
  var thisGap = gap-randomGaussian(0,50);
  if ((Math.abs(obsLeft[0].y - thisGap) < (carHeight*2 + 10))){
    leftLane = obsLeft[0].onLeftLane;
    if ((Math.abs(obsLeft[0].y - thisGap) < (carHeight+10))){
      thisGap -= carHeight;
    }
  }
  obsLeft.unshift({
            onLeftLane: leftLane,
            onRightRoad : false,
            y : thisGap,
            passed : false,
            colour: enemyColour,

  });
}

// function called to add an obstacle to the right road
function addObstacleRight(){
  var leftLane = random([false,true]);
  var thisGap = gap-randomGaussian(-10,50);
  if ((Math.abs(obsRight[0].y - thisGap) < (carHeight*2 + 10))){
    leftLane = obsRight[0].onLeftLane;
    if ((Math.abs(obsRight[0].y - thisGap) < (carHeight+10))){
      thisGap -= carHeight;
    }
  }
  obsRight.unshift({
            onLeftLane: leftLane,
            onRightRoad : true,
            y : thisGap,
            passed : false,
            colour: enemyColour,

  });
}

// function used to check collision between players and an obstacle
function checkPosition(obstacle){

    if (obstacle.onRightRoad){
      car = car2;
    } else {
      var car = car1;
    }

    if (obstacle.onLeftLane == car.onLeftLane && obstacle.y+carHeight >= car.y
      && obstacle.y <= car.y+carHeight){
        //collided
        if (!gameEnd){
          highScore.unshift(score);
        }
        gameEnd = true;
        now = millis();
        bgm.stop();
        carCrashSound.play();
      } else if (obstacle.y > car.y+carHeight && !obstacle.passed){
      //passed
      obstacle.passed = true;
      score++;
        if (obstacle.onRightRoad){
          addObstacleRight();
        } else {
          addObstacleLeft();
        }
      }

    if (obstacle.y > height+carHeight){
      if (obstacle.onRightRoad){
        obsRight.pop();
      } else {
        obsLeft.pop();
      }
    }
}

//function to draw the vehicles
function drawCar(car){
    var xValue;

    if (car.onLeftLane && car.onRightRoad){
      xValue = leftLaneRightRoad;
    } else if (car.onLeftLane && !car.onRightRoad){
      xValue = leftLaneLeftRoad;
    } else if (!car.onLeftLane && !car.onRightRoad){
      xValue = rightLaneLeftRoad;
    } else {
      xValue = rightLaneRightRoad;
    }

    fill(0);
    rect(xValue-carWidth/2-5,car.y+10,carWidth/4,carHeight/4,3,3);
    rect(xValue+carWidth/2-8,car.y+10,carWidth/4,carHeight/4,3,3);
    rect(xValue-carWidth/2-5,car.y+carHeight-35,carWidth/4,carHeight/4,3,3);
    rect(xValue+carWidth/2-8,car.y+carHeight-35,carWidth/4,carHeight/4,3,3);

    fill(car.colour);
    rect(xValue-carWidth/2,car.y,carWidth,carHeight,10,10);
    fill(255,255,255,100);

    rect(xValue-carWidth/2+5,car.y+20,carWidth-10,carHeight/5,10,10);
    rect(xValue-carWidth/2+5,car.y+60,carWidth-10,carHeight/5,10,10);

}

function reset(){

  gameEnd = false;
  obsLeft = [];
  obsRight = [];
  initialise();
  obsSpeed = 3;
  bgm.play();
  score = 0;
  start = millis();
}
//Septian Razi
//u6086829
