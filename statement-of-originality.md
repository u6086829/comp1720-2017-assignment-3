# Statement of Originality

I , Septian Razi, declare that everything I have submitted in this assignment is entirely my own
work, with the following exceptions:

## Inspiration
 Two Cars Mobile App (https://play.google.com/store/apps/details?id=com.ketchapp.twocars&hl=en)

Speeding/ Road Safety TV Commercials

## Code

Code is entirely my own.

## Assets
Sounds Used:
    - Car crash : https://www.youtube.com/watch?v=iukXU30cwj4
    - Background Music : https://www.youtube.com/watch?v=vX1xq4Ud2z8&index=6&list=PLKQWNXwX2zTkYUBbLPaTwYpDBIcFnDch3
    - Score Up : https://www.youtube.com/watch?v=GGoYBJvPEdY (not used)
